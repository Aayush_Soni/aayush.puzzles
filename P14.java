package puzzles;

import java.util.ArrayDeque;

class TreeNode {


    TreeNode leftPointer, rightPointer;
    int data;

    public TreeNode() {
        leftPointer = null;
        rightPointer = null;
        data = 0;
    }

    public TreeNode(int n) {
        leftPointer = null;
        rightPointer = null;
        data = n;
    }

    public TreeNode getLeftPointer() {
        return leftPointer;
    }

    public void setLeftPointer(TreeNode n) {
        leftPointer = n;
    }

    public TreeNode getRightPointer() {
        return rightPointer;
    }

    public void setRightPointer(TreeNode n) {
        rightPointer = n;
    }

    public int getData() {
        return data;
    }

    public void setData(int d) {
        data = d;
    }
}

class TreeN{
        private TreeNode root;

        public TreeN() {
            root = null;
        }
        public void insert(int data){
            root = insert(root,data);
        }

        private TreeNode insert(TreeNode node,int data){
            if (node == null) node = new TreeNode(data);
            else {
                if(data<=node.getData()) node.leftPointer=insert(node.leftPointer,data);
                else node.rightPointer= insert(node.rightPointer,data);
            }
            return node;
        }
        public void inorder(){
            inorder(root);
        }
        private void inorder(TreeNode tN){
            if(tN!=null){
                inorder(tN.getLeftPointer());
                System.out.print(tN.getData()+" ");
                inorder(tN.getRightPointer());
            }
        }

        public void deepestNode(){
            deepestNode(root);
        }
        private  TreeNode deepestNode(TreeNode root){
            if (root==null) return null;

            ArrayDeque <TreeNode>q = new ArrayDeque<>();
            q.offer(root);
            TreeNode node = null;
            while(!q.isEmpty()){
                for (int i = 0; i < q.size(); i++) {
                    node=q.poll();
                    if(node.leftPointer!=null){
                        q.offer(node.leftPointer);
                    }
                    if (node.rightPointer!=null){
                        q.offer(node.rightPointer);
                    }
                }
            }
            System.out.println(node.getData());
            return node;
        }
        public void maxDepth(){
            maxDepth(root);
        }
        public int maxDepth(TreeNode root){
            if(root==null){return 0;}
            return 1+Math.max(maxDepth(root.leftPointer),maxDepth(root.rightPointer));
        }


}
class mainMethodClass{
    public static void main(String[] args) {
        TreeN treeN =new TreeN();
        treeN.insert(8);
        treeN.insert(3);
        treeN.insert(5);
        treeN.insert(1);
        treeN.insert(9);
        treeN.insert(4);
        treeN.insert(7);
        treeN.insert(11);
        treeN.insert(10);
        treeN.insert(12);
        treeN.insert(13);
        treeN.inorder();
        System.out.println();
        treeN.deepestNode();
    }
}



