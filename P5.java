package puzzles;

public class P5 {
//    public  Node head1,head2;
    public static class Node{
        int data;
        Node next;
        Node(int d){
            data = d;
            next = null;
        }
    }
    public static  int intersectingNode(Node list1,Node list2){
        Node temp1 = list1;
        while(temp1!=null){
            Node temp2=list2;
            while(temp2!=null){
                if (temp1 == temp2){
                    return temp1.data;
                }
                temp2=temp2.next;
            }
            temp1=temp1.next;
        }
        return -1;
    }
    public static void printList(Node head){
        while (head!=null){
            System.out.print(head.data + " ");
            head= head.next;
        }
        System.out.println();
    }

    public static void main(String[] args) {
        Node list1 = new Node(3);
        list1.next = new Node(7);
        list1.next.next = new Node(8);
        list1.next.next.next = new Node(10);
//        list1.next.next.next.next = new Node(10);
        printList(list1);
        Node list2 =new Node(99);
        list2.next = new Node(1);
//        list2.next.next = new Node(8);
        list2.next.next = list1.next.next;
        printList(list2);
        int result = intersectingNode(list1,list2);
        System.out.println(result);

    }
}
