package puzzles;

import java.util.HashMap;
import java.util.Map;

public class FlattenDictionary {
    Map map = new HashMap();

    public Map flatteningByRecursion(Map map1,String str){
        map1.forEach((key, value) -> {
            if (key != "") {
                if (str!=""){
                    key = str + "." +key;
                }
            }else key= str;
            if (value.getClass().getName()!="java.util.HashMap"){
               map.put(key,value);
            }else flatteningByRecursion((Map)value,(String) key);
        });
        return map1;
    }
}
class Mainmeth{
    public static void main(String[] args) {
        FlattenDictionary flattenDictionary=new FlattenDictionary();
        Map mapx = new HashMap();
        Map mapy = new HashMap();
        mapx.put("baz",8);
        mapy.put("a",5);
        mapy.put("bar",mapx);
        flattenDictionary.map.put("key",3);
        flattenDictionary.map.put("foo",mapy);
        System.out.println(flattenDictionary.map);
        String st ="";
        System.out.println(flattenDictionary.flatteningByRecursion(flattenDictionary.map, st));
        System.out.println(flattenDictionary.map);
        System.out.println(mapx.getClass().getName());
    }
}
