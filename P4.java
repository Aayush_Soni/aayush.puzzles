package puzzles;

import java.util.Arrays;
import java.util.PriorityQueue;

public class P4 {
    public int meetingRooms(int[][] intervals){
        Arrays.sort(intervals,(a,b)->Integer.compare(a[0],b[0]));
        PriorityQueue<Integer> p = new PriorityQueue<>();

        for (int interval[]:intervals) {
            if (p.isEmpty()){
                p.add(interval[1]);
                continue;
            }
            if (p.peek()<=interval[0]){//same room
                p.remove();
            }
            p.add(interval[1]);
        }
        return p.size();
    }
}
