package puzzles;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class MappingOnetoOneChar {
    public boolean mapping(String s1,String s2){
        char [] ch = s1.toCharArray();
        Set s =  new HashSet();
        for (char c :  ch) {
            s.add(c);
        }
//        System.out.println(s);
        if (s1.length()!=s2.length())return false;
        else if (s.size()!=s1.length())return false;
        else return true;
    }
}
class mainmehod{
    public static void main(String[] args) {
        MappingOnetoOneChar m1 = new MappingOnetoOneChar();
        System.out.println(m1.mapping("foo","bar"));
        System.out.println();
    }
}
