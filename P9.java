package puzzles;

import java.util.Arrays;

public class P9 {
    public boolean subsetsTargetSums(int array[],int targetSum){
//        int actualTargetSum=targetSum/2;

        int j = array.length-1;

        Arrays.sort(array);
        for (int i = 0; i < array.length-1; i++) {
            int storeSum=0;
            int k = i+1;
            storeSum+=array[i];
            while (k<j){
                int ss2= 0;
                ss2=storeSum+array[i+1]+array[j];
                if(ss2==targetSum) return true;
                else if (ss2<targetSum) k++;
                else j--;
            }
        }
        return false;
    }
}
