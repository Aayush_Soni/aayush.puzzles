package puzzles;

import java.util.ArrayList;
import java.util.Stack;

public class MinimumSumTree {
    MinimumSumTree leftPointer, rightPointer;
    int data;

    public MinimumSumTree() {
        leftPointer = null;
        rightPointer = null;
        data = 0;
    }

    public MinimumSumTree(int n) {
        leftPointer = null;
        rightPointer = null;
        data = n;
    }

    public MinimumSumTree getLeftPointer() {
        return leftPointer;
    }

    public void setLeftPointer(MinimumSumTree n) {
        leftPointer = n;
    }

    public MinimumSumTree getRightPointer() {
        return rightPointer;
    }

    public void setRightPointer(MinimumSumTree n) {
        rightPointer = n;
    }

    public int getData() {
        return data;
    }

    public void setData(int d) {
        data = d;
    }
}
class Functionality{
    Stack<Integer> stk = new Stack<>();
    ArrayList<Integer> arr = new ArrayList<>();
        private MinimumSumTree root;

        public Functionality() {
            root = null;
        }
        public void insert(int data){
            root = insert(root,data);
        }
        private MinimumSumTree insert(MinimumSumTree node, int data){
            if (node == null) node = new MinimumSumTree(data);
            else {
                if(data<=node.getData()) node.leftPointer=insert(node.leftPointer,data);
                else node.rightPointer= insert(node.rightPointer,data);
            }
            return node;
        }
        public void inorder(){
            inorder(root);
        }
        private void inorder(MinimumSumTree tN){

            if(tN==null)return;

                stk.push(tN.data);
                inorder(tN.getLeftPointer());
                if (tN.getLeftPointer()==null&&tN.getRightPointer()==null){
//                    System.out.println(stk);
                    int sum=0;

                    for (int i = 0; i < stk.size(); i++) {
                       sum+= stk.get(i);

                    }
//                    System.out.println(sum);
                    arr.add(sum);

                }
                inorder(tN.getRightPointer());
                stk.pop();
        }
        public void minimumSum(){
            System.out.println(arr.get(0));
        }
}

class MyMainMethodClass {
    public static void main(String[] args) {
        Functionality functionality = new Functionality();
        functionality.insert(8);
        functionality.insert(3);
        functionality.insert(5);
        functionality.insert(1);
        functionality.insert(9);
        functionality.insert(4);
        functionality.insert(7);
        functionality.insert(11);
        functionality.insert(10);
        functionality.insert(12);
        functionality.insert(13);

        functionality.inorder();
        System.out.println();
//        System.out.println(functionality.arr);
        functionality.minimumSum();
    }
}
