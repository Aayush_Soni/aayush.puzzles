package puzzles;

import java.util.ArrayList;

public class StringPermutationsPalindrome {

    public static ArrayList<String> permutationBucket = new ArrayList<>();
    public static void stringPermutations(String inputString, String answerSoFar){

        if (inputString.length()==0) {
//           permutationBucket.add(answerSoFar);
//           System.out.println(permutationBucket);
//            for (int i = 0; i < permutationBucket.size(); i++) {
//
//            }
            permutationBucket.add(answerSoFar);
//           System.out.println(answerSoFar);
           return;
       }
        for (int i = 0; i < inputString.length(); i++) {
            char ch = inputString.charAt(i);
            String leftPart = inputString.substring(0,i);
            String rightPart = inputString.substring(i+1);
            String restOfTheString = leftPart+ rightPart;
            stringPermutations(restOfTheString,answerSoFar+ch);
        }
    }
    public static boolean palindrome(String s){
        String newString = "";
        for (int i = s.length()-1; i >=0; i--) {
            newString+=s.charAt(i);
        }
        if (newString.equals(s)) return true;
        return false;
    }
    public static boolean checkPalindrome(ArrayList<String> permutationBucket){
        for (String permutations:permutationBucket) {
            if (palindrome(permutations)) return true;
        }return false;
    }

    public static void main(String[] args) {
        String str = "aabcc";
        stringPermutations(str,"");
        System.out.println(permutationBucket);
        System.out.println(permutationBucket.size());
        System.out.println(checkPalindrome(permutationBucket));
    }
}
