package puzzles;

public class P13 {
    public Node head1,head2;
    public static class Node{
        int data;
        Node next;
        Node(int d){
            data = d;
            next = null;
        }
    }
    public static  Node reversingLinkedList(Node list1){
        Node previous = null;
        while(list1!= null){
            Node next = list1.next;
            list1.next= previous;
            previous = list1;
            list1= next;
        }
        return previous;
    }
    public static void printList(Node head){
        while (head!=null){
            System.out.print(head.data + " ");
            head= head.next;
        }
        System.out.println();
    }

    public static void main(String[] args) {
        Node list1 = new Node(3);
        list1.next = new Node(7);
        list1.next.next = new Node(8);
        list1.next.next.next = new Node(10);
//        list1.next.next.next.next = new Node(10);
        printList(list1);
//        Node list2 =new Node(99);
//        list2.next = new Node(1);
////      list2.next.next = new Node(8);
//        list2.next.next = list1.next.next;
//        printList(list2);
        Node newlist = reversingLinkedList(list1);
        printList(newlist);
    }
}
