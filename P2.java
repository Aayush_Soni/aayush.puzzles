package puzzles;

import com.sun.istack.internal.NotNull;
//import org.jetbrains.annotations.NotNull;

public class P2 {
    public String decode(@NotNull String s){
        String s1="";
        int count;
        for (int i = 0; i < s.length(); i=i+2) {
            count = Integer.parseInt(String.valueOf(s.charAt(i)));
            for (int j = 0; j <count ; j++) {
                s1+=s.charAt(i+1);

            }

        }return s1;
    }

    public String encode(@NotNull String s){
        String s1="";
        int count;
        for (int i =0;i<s.length();i++) {
            count =1;
            while (i+1<s.length() && s.charAt(i)==s.charAt(i+1)) {
                count++;
                i++;
            }
            s1+=s.valueOf(count)+s.charAt(i);
        }return s1;
    }

}
