package puzzles;

import java.util.ArrayList;

import java.util.ArrayList;
import java.util.LinkedList;
public class P16SparseArray {
    ArrayList<Integer> sparseArray = new ArrayList<>(20);


    public void toSparseArray(int []inputArray,int size){
        size = inputArray.length;
        for (int i = 0; i < size; i++) {
            if(inputArray[i]!=0) sparseArray.set(i, inputArray[i]);
        }
//            System.out.println(sparseArray);
    }
    public int get(int i ){
        return sparseArray.get(i);
    }
    public void set(int i ,int val){
        sparseArray.set(i,val);
    }

    public static void main(String[] args) {
        P16SparseArray p16SparseArray= new P16SparseArray();
        int []inpArray = {2,1,0,0,0,4,5,3,0,0,0,2};
        for (int i = 0; i < 20; i++) {
            p16SparseArray.sparseArray.add(0);
        }
        p16SparseArray.toSparseArray(inpArray, 2);
        System.out.println(p16SparseArray.get(4));
        System.out.println(p16SparseArray.get(3));
        p16SparseArray.set(3,12);
        System.out.println(p16SparseArray.get(3));


    }


}
