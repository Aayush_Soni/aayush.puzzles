package puzzles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class P11 {
//    ArrayList<Integer> leftPartOfArray ;
//    ArrayList<Integer> rightPartOfArray ;
//    ArrayList<Integer> restOfTheArray ;
//    public void permutations(ArrayList<Integer> al,ArrayList<Integer> answerSoFar){
//
//        if (al.size()==0){
//            System.out.println(answerSoFar);
//            return;
//        }
//        for (int i = 0; i < al.size(); i++) {
//            int a = al.get(i);
//            for (int j=0;j<i;j++) {
//                leftPartOfArray=new ArrayList<>();
//                leftPartOfArray.add(al.get(j));
//            }
//            for (int k = i+1; k <al.size(); k++) {
//                rightPartOfArray= new ArrayList<>();
//                rightPartOfArray.add(al.get(k));
//            }
//
//            leftPartOfArray.addAll(rightPartOfArray);
//            restOfTheArray= new ArrayList<>(leftPartOfArray);
//
//            answerSoFar.add(a);
//            permutations(restOfTheArray,answerSoFar);
//        }
//    }
    public List<List<Integer>> finalPermutation(int[] arrayOfNums){
        LinkedList<List<Integer>> result = new LinkedList<List<Integer>>();
        int resultSize;
        result.add(new ArrayList<Integer>());
//        System.out.println(result);

        for (int num:arrayOfNums) {
            resultSize= result.size();
//            System.out.println(result);
            while(resultSize>0){
                List<Integer> permutations = result.pollFirst();
//                System.out.println(result);
                for (int i = 0; i <= permutations.size(); i++) {
                    List<Integer> newPermutations = new ArrayList<Integer>(permutations);
                    newPermutations.add(i,num);
                    result.add(newPermutations);
                }
                resultSize--;
            }

        }
        System.out.println(result);
        return result;
    }
}
