package puzzles;

public class RotatingLinkedListByKthTerm {
    public static class Node{
        int data;
        Node next;
        Node(int d){
            data = d;
            next=null;
        }
    }
    public static Node rotateByKTerm(Node head,int k){
        if (head ==null)return head;
        Node temp = head;
        int len=0;
        while(temp.next!=null){
            temp =temp.next;
            len++;
        }
        if (k==0||k==len)return head;
        Node current = head;
        int count=1;
        while(count<k-1 && current != null){
            current = current.next;
            count++;
        }
        Node kthNode = current;
        temp.next=head;
        head=kthNode.next;
        kthNode.next=null;
        return head;
    }
    public static void printList(Node head){
        while (head!=null){
            System.out.print(head.data + " ");
            head= head.next;
        }
        System.out.println();
    }

    public static void main(String[] args) {
        Node list1 = new Node(1);
        list1.next = new Node(2);
        list1.next.next = new Node(3);
        list1.next.next.next = new Node(4);
        list1.next.next.next.next = new Node(5);
        printList(list1);
        Node newlist = rotateByKTerm(list1,3);
        printList(newlist);
    }
}
