package puzzles;

public class P15 {
    public int numOfIslands(int [][] matrix) {
        int count = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                if (matrix[i][j] == 1) {
                    createZero(matrix, i, j);
                    count++;
                }
            }
        }
        return count;

    }
    public void createZero(int [][] matrix,int i ,int j){
        if (i>=0&& j>=0 && i<matrix.length && j< matrix[0].length&& matrix[i][j]==1){
            matrix[i][j]=0;
            createZero(matrix,i+1,j);
            createZero(matrix,i-1,j);
            createZero(matrix,i,j+1);
            createZero(matrix,i,j-1);
        }
    }
}
