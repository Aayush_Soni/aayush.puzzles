package puzzles;

public class P10 {
    public int maxProfit(int array[]){
        int firstBuy = 0;
        int maxProfit = 0;
        for (int i = 0; i < array.length-1; i++) {
            firstBuy=array[i];
            int j = i+1;
            while(j< array.length){
                if (array[j]-firstBuy>maxProfit) maxProfit=array[j]-firstBuy;
                j++;
            }

        }
        return maxProfit;
    }
}
