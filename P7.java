package puzzles;

import java.util.Arrays;

public class P7 {
    public int returnLargestProduct(int array[]){
        int j= array.length;
        Arrays.sort(array);
        int productOfFirstTwoAndLast=array[0]*array[1]*array[j-1];
        int productOfLastThree = array[j-1]*array[j-2]*array[j-3];
        if(productOfLastThree>productOfFirstTwoAndLast)return productOfLastThree;
        else return productOfFirstTwoAndLast;
    }
}
