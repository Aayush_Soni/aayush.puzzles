package puzzles;

import java.util.ArrayList;
import java.util.HashSet;

public class LongestSubarray {
    public  int allSubArrays(int [] arr) {
        int i = 0, j = 1, max = 0, currentLength = 1;
        max = Math.max(max, currentLength);
        HashSet<Integer> set = new HashSet<>();
        set.add(arr[0]);

        while (i < arr.length - 1 && j < arr.length) {
            if (!set.contains(arr[j])) {
                currentLength++;
                set.add(arr[j++]);
            }
            else {
                set.remove(arr[i++]);
                currentLength--;
            }
        }

        return Math.max(currentLength, max);
}
}
class  mnmetod{
    public  static void main(String[] args) {
        int[] arr = {5, 1, 3, 5, 2, 3, 4, 1};
        LongestSubarray l = new LongestSubarray();
        System.out.println(l.allSubArrays(arr));

    }
}
